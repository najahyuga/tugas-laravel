@extends('adminlte.master')
@section('judul')
    Halaman Utama
@endsection
@section('content')
<h1>Buat Acoount Baru!</h1>
<h2>Sign Up Form</h2>

<form method="POST" action="/come">
    @csrf

    <label for="firstname">First Name:</label><br><br>
    <input type="text" id="firstname" name="firstname" required>

    <br><br>

    <label for="lastname">Last Name:</label><br><br>
    <input type="text" id="lastname" name="lastname">

    <br><br>
    <label>Gender:</label><br><br>
    <input type="radio" name="gender" id="male">
    <label for="male">Male</label><br>

    <input type="radio" name="gender" id="female">
    <label for="female">Female</label><br>

    <input type="radio" name="gender" id="other">
    <label for="other">Other</label><br><br>

    <label for="nationality">Nationality:</label><br><br>
    <select name="nationality" id="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="malaysia">Malaysia</option>
        <option value="singapore">Singapore</option>
        <option value="australia">Australia</option>
    </select>
    <br><br>

    <label>Language Spoken:</label><br><br>
    <input type="checkbox" name="indonesia" id="language">
    <label for="language">Bahasa Indonesia</label>
    <br>
    <input type="checkbox" name="english" id="english">
    <label for="english">English</label>
    <br>
    <input type="checkbox" name="other" id="otherlang">
    <label for="otherlang">Other</label>
    <br><br>

    <label for="bio">Bio:</label><br><br>
    <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
    <br>

    <button type="submit" value="kirim">Sign Up</button>
    <button type="reset" value="reset">Reset</button>

</form>

@endsection