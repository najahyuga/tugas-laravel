@extends('adminlte.master')
@section('judul')
    <h2>Show Post {{$cast->nama}}</h2>  
@endsection
@section('content')  
    <h4>Nama : {{$cast->nama}}</h4>
    <p>Umur : {{$cast->umur}}</p>
    <p>Bio : {{$cast->bio}}</p>
@endsection