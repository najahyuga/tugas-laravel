@extends('adminlte.master')
@section('judul')
    Tambah Data
@endsection
@section('content')
    <form method="POST" action="/cast">
        @csrf
        
        <div class="form-group">
            <label for="nama">Nama Cast :</label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">Umur :</label>
            <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio :</label>
            <textarea name="bio" class="form-control" cols="30" rows="10" placeholder="Masukkan Bio"></textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
@endsection