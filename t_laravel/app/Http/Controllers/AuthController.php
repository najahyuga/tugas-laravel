<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Auth()
    {
        return view('register');
    }

    public function kirim(Request $request)
    {
        $depan = $request['firstname'];
        $belakang = $request['lastname'];

        return view('come', compact('depan', 'belakang'));
    }
}
